function createDataset(fields, constraints, sortFields) {
	try {
		return processResult(callService(fields, constraints, sortFields));
	} catch(e) {
		return processErrorResult(e, constraints);
	}
}

function callService(fields, constraints, sortFields) {
	var databaseData = data();
	var resultFields, queryClauses;

	resultFields = getOutputFields(databaseData.outputValues);
	queryClauses = verifyConstraints(databaseData, constraints);

	var result = DatabaseManager.select(databaseData.fluigService, databaseData.operation, resultFields, queryClauses, databaseData.extraParams);

	return result;
}

function defineStructure() {
	var databaseData = data();
	var columns = getOutputFields(databaseData.outputValues);

	for (column in columns) {
		var columnName = removeInvalidChars(columns[column]);
		if (!DatabaseManager.isReservedWord(columnName)) {
			addColumn(columnName);
		} else {
			addColumn('ds_' + columnName);
		}
	}
	if (databaseData.extraParams.key) {
		setKey([databaseData.extraParams.key]);
	}
}

function onSync(lastSyncDate) {
	var databaseData = data();
	var synchronizedDataset = DatasetBuilder.newDataset();

	try {
		var resultDataset = processResult(callService());
		if (resultDataset != null) {
			var values = resultDataset.getValues();
			for (var i = 0; i < values.length; i++) {
				if (databaseData.extraParams.key) {
					synchronizedDataset.addOrUpdateRow(values[i]);
				} else {
					synchronizedDataset.addRow(values[i]);
				}
			}
		}

	} catch(e) {
		log.info('Dataset synchronization error : ' + e.message);

	}
	return synchronizedDataset;
}

function verifyConstraints(params, constraints) {
	var allConstraints = new Array();

	if (constraints != null) {
		for (var i = 0; i < constraints.length; i++) {
			if (constraints[i].getFieldName().toLowerCase() == 'sqllimit') {
				params.extraParams['limit'] = constraints[i].getInitialValue();
			} else {
				allConstraints.push(constraints[i]);
			}
		}
	}

	if (allConstraints.length == 0) {
		for (i in params.inputValues) {
			for (j in params.inputValues[i]) {
				var param = params.inputValues[i][j];
				var constraint = DatasetFactory.createConstraint(param.fieldName, param.initialValue, param.finalValue, param.constraintType);
				constraint.setLikeSearch(param.likeSearch);
				constraint.setFieldType(DatasetFieldType.valueOf(param.fieldType));
				allConstraints.push(constraint);
			}
		}
	}
	return allConstraints;
}

function getOutputFields(outputValues) {
	var outputFields = new Array();
	if (outputValues != null) {
		for (field in outputValues) {
			if (outputValues[field].result) {
				outputFields.push(field);
			}
		}
	}
	return outputFields;
}

function processResult(result) {
	var databaseData = data();
	var dataset = DatasetBuilder.newDataset();
	var columns = getOutputFields(databaseData.outputValues);

	for (column in columns) {
		dataset.addColumn(columns[column]);
	}

	for (var i = 0; i < result.size(); i++) {
		var datasetRow = new Array();
		var item = result.get(i);
		for (param in columns) {
			datasetRow.push(item.get(columns[param]));
		}
		dataset.addRow(datasetRow);
	}

	return dataset;
}

function processErrorResult(error, constraints) {
	var dataset = DatasetBuilder.newDataset();

	dataset.addColumn('error');
	dataset.addRow([error.message]);

	return dataset;
}

function removeInvalidChars(columnName) {
	var invalidChars = '#';
	var newChar = '_';
	for (var i = 0; i < invalidChars.length; i++) {
		columnName = columnName.split(invalidChars[i]).join(newChar);
	}

	return columnName;
}

function data() {
	return {
  "fluigService" : "ERP_PROTOTIPO",
  "operation" : "SCR010",
  "tableType" : "TABLE",
  "parameters" : [ ],
  "inputValues" : {
    "CR_APRORI" : [ ],
    "CR_APROV" : [ ],
    "CR_AVISO" : [ ],
    "CR_DATALIB" : [ ],
    "CR_EMISSAO" : [ ],
    "CR_ESCALON" : [ ],
    "CR_ESCALSP" : [ ],
    "CR_FILIAL" : [ ],
    "CR_FLUIG" : [ ],
    "CR_GRUPO" : [ ],
    "CR_ITGRP" : [ ],
    "CR_LIBAPRO" : [ ],
    "CR_MOEDA" : [ ],
    "CR_NIVEL" : [ ],
    "CR_NUM" : [ ],
    "CR_OBS" : [ ],
    "CR_PRAZO" : [ ],
    "CR_STATUS" : [ ],
    "CR_TIPO" : [ ],
    "CR_TIPOLIM" : [ ],
    "CR_TOTAL" : [ ],
    "CR_TXMOEDA" : [ ],
    "CR_ULTAVIS" : [ ],
    "CR_USER" : [ ],
    "CR_USERLIB" : [ ],
    "CR_USERORI" : [ ],
    "CR_VALLIB" : [ ],
    "CR_WF" : [ ],
    "D_E_L_E_T_" : [ {
      "fieldName" : "D_E_L_E_T_",
      "initialValue" : "*",
      "finalValue" : "",
"constraintType" :  ConstraintType.MUST_NOT,
      "likeSearch" : false,
      "fieldType" : "STRING"
    } ],
    "R_E_C_N_O_" : [ ]
  },
  "inputAssignments" : { },
  "outputValues" : {
    "CR_APRORI" : {
      "result" : true,
      "type" : "varchar"
    },
    "CR_APROV" : {
      "result" : true,
      "type" : "varchar"
    },
    "CR_AVISO" : {
      "result" : true,
      "type" : "varchar"
    },
    "CR_DATALIB" : {
      "result" : true,
      "type" : "varchar"
    },
    "CR_EMISSAO" : {
      "result" : true,
      "type" : "varchar"
    },
    "CR_ESCALON" : {
      "result" : true,
      "type" : "varchar"
    },
    "CR_ESCALSP" : {
      "result" : true,
      "type" : "varchar"
    },
    "CR_FILIAL" : {
      "result" : true,
      "type" : "varchar"
    },
    "CR_FLUIG" : {
      "result" : true,
      "type" : "varchar"
    },
    "CR_GRUPO" : {
      "result" : true,
      "type" : "varchar"
    },
    "CR_ITGRP" : {
      "result" : true,
      "type" : "varchar"
    },
    "CR_LIBAPRO" : {
      "result" : true,
      "type" : "varchar"
    },
    "CR_MOEDA" : {
      "result" : true,
      "type" : "float"
    },
    "CR_NIVEL" : {
      "result" : true,
      "type" : "varchar"
    },
    "CR_NUM" : {
      "result" : true,
      "type" : "varchar"
    },
    "CR_OBS" : {
      "result" : false,
      "type" : "varbinary"
    },
    "CR_PRAZO" : {
      "result" : true,
      "type" : "varchar"
    },
    "CR_STATUS" : {
      "result" : true,
      "type" : "varchar"
    },
    "CR_TIPO" : {
      "result" : true,
      "type" : "varchar"
    },
    "CR_TIPOLIM" : {
      "result" : true,
      "type" : "varchar"
    },
    "CR_TOTAL" : {
      "result" : true,
      "type" : "float"
    },
    "CR_TXMOEDA" : {
      "result" : true,
      "type" : "float"
    },
    "CR_ULTAVIS" : {
      "result" : true,
      "type" : "varchar"
    },
    "CR_USER" : {
      "result" : true,
      "type" : "varchar"
    },
    "CR_USERLIB" : {
      "result" : true,
      "type" : "varchar"
    },
    "CR_USERORI" : {
      "result" : true,
      "type" : "varchar"
    },
    "CR_VALLIB" : {
      "result" : true,
      "type" : "float"
    },
    "CR_WF" : {
      "result" : true,
      "type" : "varchar"
    },
    "D_E_L_E_T_" : {
      "result" : true,
      "type" : "varchar"
    },
    "R_E_C_N_O_" : {
      "result" : false,
      "type" : "int"
    }
  },
  "outputAssignments" : { },
  "extraParams" : {
    "queryOrderField" : "CR_NUM",
    "queryOrderDirection" : "DESC"
  }
}
;
}