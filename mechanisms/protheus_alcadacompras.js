function resolve(process,colleague){
    // ----------------------------------------------------------------------------------------
    // Regras para atribuição da atividade para o usuário responsável
    // ----------------------------------------------------------------------------------------
    var userList             = new java.util.ArrayList();
    var userMail             = '';
    var dsPedidosPendentes     = DatasetFactory.getDataset("ds_PedidoPendente"                , null, null, ['CR_NUM', 'CR_USER']);
    var dsAprovadores         = DatasetFactory.getDataset("ds_Aprovadores_Pedidocompra"    , null, null, null);
    var userCode             = (dsPedidosPendentes.getValue(0, 'CR_USER')).trim();
    var numPedido            = (dsPedidosPendentes.getValue(0, 'CR_NUM')).trim();
    
    var codSup                 = '';
    var supMail             = '';

    log.info('usercodeigual=' + userCode);
    log.info('numPedido=' + numPedido);
    log.info('tamanho='+dsAprovadores.values.length);
    for(var g = 0; g < dsAprovadores.values.length; g++)
    {

    	 log.info((dsAprovadores.getValue(g, 'AK_USER')).trim() +'com'+ userCode);
        if((dsAprovadores.getValue(g, 'AK_USER')).trim() == userCode)
        {
       
            userMail = (dsAprovadores.getValue(g, 'AK_EMAIL')).trim();
            codSup   = (dsAprovadores.getValue(g, 'AK_APROSUP')).trim();
            break;
        }
    }
   
    var c1             = DatasetFactory.createConstraint('mail', userMail, userMail, ConstraintType.MUST);
    var dsColleague    = DatasetFactory.getDataset("colleague", null, new Array(c1), null);

    log.info('ak superior = ' + codSup);
    
    log.info('aprovador = ' + dsColleague.getValue(0, 'colleaguePK.colleagueId'));
    log.info('ak superior = ' + codSup);
    log.info('ak superior = ' + userMail);
    userList.add(dsColleague.getValue(0, 'colleaguePK.colleagueId'));

    // Pega superior
    for(var g = 0; g < dsAprovadores.values.length; g++)
    {

        if((dsAprovadores.getValue(g, 'AK_COD')).trim() == codSup)
        {
       
            supMail = (dsAprovadores.getValue(g, 'AK_EMAIL')).trim();
            break;
        }
    }
    
    log.info('procurando com email = ' + supMail);
    var c2             = DatasetFactory.createConstraint('mail', supMail, supMail, ConstraintType.MUST);
    var dsColleague1   = DatasetFactory.getDataset("colleague", null, new Array(c2), null);
   // userList.add(dsColleague1.getValue(0, 'colleaguePK.colleagueId'));
    // ----------------------------------------------------------------------------------------
    // Fim das regras para atribuição da atividade para o usuário responsável
    // ----------------------------------------------------------------------------------------

    log.info('superior = ' + dsColleague1.getValue(0, 'colleaguePK.colleagueId'));
    
    for(y=0;y<userList.size();y++)
    	{
    		log.info('###'+userList.get(y));
    	}
   // userList.add('wpyr5r4yhg6acah11530103353600');
   //userList.add('57suwprkph83oztn1469466819588');
    return userList;
}