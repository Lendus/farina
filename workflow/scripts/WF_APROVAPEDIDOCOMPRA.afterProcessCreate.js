function afterProcessCreate(processId)
{
	//--------------------------------------------------------------------------------------------------------
	log.info('teste111');
	var dsPedidosPendentes 	= DatasetFactory.getDataset("ds_PedidoPendente", null, null, ['CR_NUM', 'CR_USER']);
	log.info('teste222');
	var clientService 		= fluigAPI.getAuthorizeClientService();
	var numDoc 				= (dsPedidosPendentes.getValue(0, 'CR_NUM')).trim();
	var userAprov 			= (dsPedidosPendentes.getValue(0, 'CR_USER')).trim();
	var restEnd 			= 'CONSULTASCR?CDOCDE='+ numDoc +'&CDOCAT='+ numDoc +'&CSTATUS=02&CFILP=0101&CAPROV='+ userAprov +'';
	
	log.info('url para o protheus = ' + restEnd);
	
	var data = {
	         companyId : ''+getValue('WKCompany')+'',
	         serviceCode : 'Consulta Pedidos Pendentes',
	         endpoint : restEnd,
	         method : 'GET',
	         params : {  },
	         timeoutService: '10000'
	  }
	log.info("DATA");	
	log.info(data);

	log.info("DATA stringify");
	log.info(JSON.stringify(data));

	var jsonProtheus 	= clientService.invoke(JSON.stringify(data));
	var dadosProtheus 	= JSON.parse(jsonProtheus.getResult());
	//-----------------------------------------------------------------------------------------------------
	var dataIn		= dadosProtheus[0].CR_EMISSAO;
	var ano 		= dataIn.substring(0,4);
	var mes 		= dataIn.substring(4,6);
	var dia 		= dataIn.substring(6,8);
	var numPedido 	= (dadosProtheus[0].CR_NUM).trim();
	var userCode 	= (dadosProtheus[0].CR_USER).trim();
		
	// Carrega campos do header
	hAPI.setCardValue('Emissao'		, dia + "/" + mes + "/" + ano);
	hAPI.setCardValue('AprovOri'	, dadosProtheus[0].CR_USER);
	hAPI.setCardValue('Documento'	, dadosProtheus[0].CR_NUM);
	hAPI.setCardValue('Total'		, dadosProtheus[0].CR_TOTAL);
	hAPI.setCardValue('Fornecedor'	, dadosProtheus[0].C7ITENS[0].C7_NOMEFORNECE);
	hAPI.setCardValue('CentroCusto'	, dadosProtheus[0].C7ITENS[0].C7_CC);
	
	// Carrega itens (pai e filho)
	for (var i = 0; i < dadosProtheus[0].C7ITENS.length; i++) {
		var childData = new java.util.HashMap();

	    childData.put("produto"				, dadosProtheus[0].C7ITENS[i].C7_PRODUTO);
	    childData.put("descricao"			, dadosProtheus[0].C7ITENS[i].C7_DESCRI);
	    childData.put("quant"				, dadosProtheus[0].C7ITENS[i].C7_QUANT);
	    childData.put("UM"					, dadosProtheus[0].C7ITENS[i].C7_UM);
	    childData.put("precUnit"			, dadosProtheus[0].C7ITENS[i].C7_PRECO);
	    childData.put("valTotal"			, dadosProtheus[0].C7ITENS[i].C7_TOTAL);
	    
	    hAPI.addCardChild("tbConsultoria"	, childData);
	}
	
	// Informa o protheus através do campo CR_FLUIG, que o pedido já está disponível para ser liberado no Fluig
	// http://172.16.1.14:9060/rest/ATUPED?CFILP=0101&CDOCUMENTO=053214&CUSER=000029
	restEnd = 'ATUPED?CFILP=0101&CDOCUMENTO=' + numPedido + '&CUSER=' + userAprov;
	
	log.info('rhyan url para o protheus - atualiza pedido = ' + restEnd);
	
	var data = {
	         companyId : '' + getValue('WKCompany') + '',
	         serviceCode : 'Atualiza Pedido Protheus',
	         endpoint : restEnd,
	         method : 'GET',
	         params : {  },
	         timeoutService: '10000'
	  }
	
	jsonProtheus 	= clientService.invoke(JSON.stringify(data));
	dadosProtheus 	= JSON.parse(jsonProtheus.getResult());
}