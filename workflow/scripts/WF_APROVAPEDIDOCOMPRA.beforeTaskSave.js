function beforeTaskSave(colleagueId,nextSequenceId,userList){
	 var atv    = getValue("WKNumState");
	 log.info('Entrou Before');
	 log.info('Atividade :'+ atv);
	if (atv == 10) {
		var numDocumento = hAPI.getCardValue('Documento');
		var codFilial 	 = '0101';
		var aprovOri	 = hAPI.getCardValue('AprovOri'); // Código do usuário que deveria aprovar o documento
		var aprov 	 	 = ''  // Código do usuário que está fazendo a aprovação
		var textoObs 	 = hAPI.getCardValue('obs_apr_validacaoProposta');
		var tipoOper 	 = '' // hAPI.getCardValue('ValidacaoAprov');

		var emailUserAtual 	= fluigAPI.getUserService().getCurrent().getEmail();
		var dsAprovadores 	= DatasetFactory.getDataset("ds_Aprovadores_Pedidocompra"	, null, null, null);
		
		var contador = 0;
		
		for(var g = 0; g < dsAprovadores.values.length; g++)
		{
	        if((dsAprovadores.getValue(g, 'AK_EMAIL')).trim() == emailUserAtual.trim())
	        {
	        	aprov = (dsAprovadores.getValue(g, 'AK_USER')).trim();
	        	contador++;
	        }
	        
	        if((dsAprovadores.getValue(g, 'AK_EMAIL')).trim() == aprovOri.trim())
	        {
	        	aprovOri = (dsAprovadores.getValue(g, 'AK_USER')).trim();
	        	contador++;
	        }
	        
	        if (contador == 2)
	        	break;
	        
	    }

		if(hAPI.getCardValue('OK') == 'SIM')
			tipoOper = '1'
		else
			tipoOper = '2'
		
		log.info('Ini Parameters');
		log.info(numDocumento);
		log.info(aprovOri);
		log.info(aprov);
		log.info(textoObs);
		log.info(tipoOper);
		log.info('Fim Parameters');

		var clientService 	= fluigAPI.getAuthorizeClientService();
		var restEnd 		= 'LIBPED?CTIPOOP='+tipoOper.trim()+'&CFILP=0101&CDOCUMENTO='+numDocumento.trim()+
								'&CUSERORI='+aprovOri.trim()+'&CUSER='+aprov.trim()+'&COBS='+textoObs.trim();
		
		log.info('url para o protheus - libera pedido = ' + restEnd);
		var ret = ''
		try{
			var data = {
			         companyId : '' + getValue('WKCompany') + '',
			         serviceCode : 'Libera Pedidos Compra',
			         endpoint : restEnd,
			         method : 'GET',
			         params : {  },
			         timeoutService: '10000'
			  }
			
			jsonProtheus 	= clientService.invoke(JSON.stringify(data));
			dadosProtheus 	= JSON.parse(jsonProtheus.getResult());
			ret = dadosProtheus.retorno;
		}
		catch(e){
			ret = 'erro';
		}
		
		log.info('Valor do DATA: ' + ret);
		if(ret=="Registro liberado com sucesso!" || ret != undefined){
			hAPI.setCardValue('RetorAprov','Aprovado');
			log.info('Entrou Aprov');
		}else{
			hAPI.setCardValue('RetorAprov','Reprovado');
			log.info('Entrou Reprov');
		}
			
		log.info('valor do campo hidden: '+ hAPI.getCardValue('RetorAprov'));
	}
	log.info('saiu do evento');
}